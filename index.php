<?php
	require_once('lib/app.php');
	$query = "SELECT * FROM image";
	$result = mysqli_query($link, $query);
	$images = array();
	while($row = mysqli_fetch_assoc($result)){
		//$row['hobbies'] = explode(',',$row['hobbies']);
		$images[] = $row;
	}
	 
?>
<!DOCTYPE html>
<html>
<head>
	<title>Images</title>
</head>
<body>
<!-- messages -->
<?php if(isset($_SESSION['msg_success']) AND !empty($_SESSION['msg_success'])):?>
	<h4 style="color:green"><?php echo $_SESSION['msg_success']; unset($_SESSION['msg_success'])?></h4>
<?php endif; ?>
<?php if(isset($_SESSION['msg_error']) AND !empty($_SESSION['msg_error'])):?>
	<h4 style="color:red"><?php echo $_SESSION['msg_error']; unset($_SESSION['msg_error'])?></h4>
<?php endif; ?>
<!-- ./end of msg -->
<nav>
	<li><a href="create.php">Add new Image</a></li>
	<li><a href="gallery.php">Imsge Gallery</a></li>
	<li><a href="gallary-slider.php">Slider Gallery</a></li>
</nav>
<h1>Images</h1>
<table border="1">
	<thead>
		<tr>
			<th>id</th>
			<th>image</th>
			<th>Image Name</th>
			<th>Image Type</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
	<?php if(count($images)):?>
		<?php foreach($images as $img):?>
		<tr>
			<td><?php echo $img['id']  ?></td>
			<td><img src=<?php echo $img['img_path'] ?> height="200px" width="200px"></td>
			<td><?php echo $img['img_path'] ?></td>
			<td><?php echo $img['img_type'] ?></td>
			<td>
				<a href="delete.php?id=<?php echo $img['id'] ?>"> delete</a> |
				<a href="view.php?id=<?php echo $img['id'] ?>">view</a> |
				<a href="edit.php?id=<?php echo $img['id']  ?>">Edit</a>

			</td>
		</tr>
		<?php endforeach;?>
	<?php else:?>
		<tr><td colspan="5">No image found!</td></tr>
	<?php endif;?>
	</tbody>
</table>
</body>
</html>