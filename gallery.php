<?php
	require_once('lib/app.php');
	$query = "SELECT * FROM image";
	$result = mysqli_query($link, $query);
	$images = array();
	while($row = mysqli_fetch_assoc($result)){
		//$row['hobbies'] = explode(',',$row['hobbies']);
		$images[] = $row;
	}
	 
?>
<!DOCTYPE html>
<html>
<head>
	<title>Images</title>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0-alpha1/jquery.min.js"></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</head>
<body>

<nav>
	<li><a href="create.php">Add new Image</a></li>
</nav>
<h1 class="text-center">Image gallary</h1>

	
	<?php if(count($images)):?>
		<div class="row">
		<?php foreach($images as $img):?>
		
		  <div class="col-md-3" style="margin-bottom:20px">
		  	<img src=<?php echo $img['img_path'] ?> height="250px" width="300" >
		  </div>
		<?php endforeach;?>
		</div>
	<?php else:?>
		No image found!
	<?php endif;?>

</body>
</html>