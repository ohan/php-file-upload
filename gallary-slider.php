<?php
	require_once('lib/app.php');
	$query = "SELECT * FROM image";
	$result = mysqli_query($link, $query);
	$images = array();
	while($row = mysqli_fetch_assoc($result)){
		//$row['hobbies'] = explode(',',$row['hobbies']);
		$images[] = $row;
	}
	 
?>
<!DOCTYPE html>
<html>
<head>
	<title>Images</title>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0-alpha1/jquery.min.js"></script>
	
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</head>
<body>

<nav>
	<li><a href="create.php">Add new Image</a></li>
	<li><a href="index.php">home</a></li>
</nav>
<h1 class="text-center">Image gallary</h1>
<div class="row">
<div id="carousel-example-generic" class="carousel slide col-md-5" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
  	<?php for($i = 0; $i<count($images); $i++):?>
    <li data-target="#carousel-example-generic" data-slide-to="<?php echo $i ?>" class="<?php echo $i==0?'active':''?>"></li>
    <?php endfor; ?>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
  <?php $i=0; foreach ($images as $img): ?> 
    <div class="item <?php echo $i==0?'active':''?>">
      <img src="<?php echo $img['img_path']; ?>" alt="...">
      <div class="carousel-caption">
        ...
      </div>
    </div>
    <?php  $i++ ?>
  <?php endforeach;?>
  </div>

  <!-- Controls -->
  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
</div>

</body>
</html>